# SSH server
Open Git Bash
cd Desktop/
chmod 400 dorna_lab.pem
ssh -i "dorna_lab.pem" ec2-user@ec2-18-219-220-240.us-east-2.compute.amazonaws.com

# Get the latest version  
cd dorna_lab_app/dorna_lab
sudo git reset --hard HEAD
sudo git pull

# Boot the server
cd dorna_lab_app/dorna_lab/dorna_lab
nohup sudo /home/ec2-user/dorna_lab_app/env/bin/python3 application.py