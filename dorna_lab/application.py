from pkg_resources import resource_filename
from flask import Flask, render_template
# thread
import json

class gui(object):
	"""docstring for ClassName"""
	def __init__(self):
		super(gui, self).__init__()

	def run(self, debug=True, host="127.0.0.1" ,port=5000):
		app = Flask(__name__)
		app.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'

		@app.route('/')
		def index():
			# read json file
			try:
				with open(resource_filename("dorna_lab", "cmd.json")) as f:
					cmd = json.load(f)
			except:
				with open("cmd.json") as f:
					cmd = json.load(f)
			data = {"host_ip": host, "port": port, "version": "test"}

			return render_template('index.html', data = data, cmd = cmd)

		app.run(debug=debug, host=host, port = port)

if __name__ == '__main__':
	gui = gui().run(debug=False, host="0.0.0.0" ,port= 80)
