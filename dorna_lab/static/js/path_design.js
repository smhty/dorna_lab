// change point

var ND = 8;
var xyz_names = ["x","y","z","a","b","c","d","e"]

function set_ND(n){
	if((n!=8 && n!=7 && n!=6 && n!=5) || n==ND)
		return 0;
	ND = n;

	$( ".path_design_point_form , .position_form , .jog_form , .set_joint_form").each(function(e){
		if($(this).attr("data-key") < ND){
			$(this).prop("style").display = "";
		}
		else{
			$(this).prop("style").display = "none";
		}
	});
	return 0;
}

set_ND(5);

function set_cmd_to_robot(){
	let p = position("joint");

	$( ".path_design_point_prm_v" ).each( function(e) {
		let space = $(this).attr("data-space"); 
		if (space == "joint"){
			$(this).val(p[$(this).attr("data-key")]);
			$(this).trigger("change");
		}
	});
}

$( ".path_design_point_prm_v" ).on("change", function(e) {
	
	let space = $(this).attr("data-space"); 
	if (space == "joint"){
		let joint = []
		let cmd = {};
		for (let i =0; i < ND ; i++) {
			joint.push(+$(`.path_design_point_prm_v[data-key=j${i}]`).prop("value"))
			cmd["j" + i] = joint[i];
		}
		//console.log("joint: ", joint)
		chain.control_cmd.set_cmd(cmd)
	}
	else{
		let xyzab = []
		let cmd = {};
		for (let i =0; i < ND ; i++) {
			xyzab.push(+$( ".path_design_point_prm_v[data-key=" + xyz_names[i] + "]" ).prop("value"))
			cmd[xyz_names[i]] = xyzab[i];
		}

		console.log("xyzab: ", xyzab)
		chain.control_cmd.set_cmd(cmd);
	}

});

// control type
$('.path_design_point_ct_r').on("change", function(e){
	chain.controller.set_control_mode(this.value)
})

$(".path_design_move_b").on("click", function(e){
	// initialize msg
	let msg = {
		"cmd" : $(this).attr("data-cmd"),
		"rel": 0
	}

	// sync position
	if ($(".path_design_sync_c").prop("checked")){
		msg = {...msg,...position("joint"), ...position("xyz")}
	}

	// prm
	/*
	$(".path_design_prm_c" ).each(function(e){
		if ($(this).prop("checked")){
			let key = $(this).attr("data-key")
			msg[key] = Number($(`.path_design_prm_v[data-key=${key}]`).prop("value"))
		}
	})
	*/

	//show div
	/*
	command = chain.add(msg);

	// update list
	update_path_design_list(command,command +": "+ $(this).text())
	$(`.path_design_program_list_b[data-id=${command}]`).click()
	*/
	chain.create_by_id(msg,$(this).attr("data-cmd"))//$(this).text())
	
})


// select a move
$(".path_design_program_list").on("click", ".path_design_program_list_b", function(e){

	// get id
	let id = Number($(this).attr("data-id"))

	// select first point
	chain.list[id].select()
})


// select from list
$('.path_design_point_s').change(function(){
	let id = Number($(this).attr("data-id"))
	let point_id = Number($(this).prop("value"))
	console.log(point_id)
	// select first point
	chain.list[id].select(point_id + 1)      
})

// save
$(".path_design_move_save_b").on("click", function(e){
	//
	let id = Number($(this).attr("data-id"))
	// read vel, ...
	chain.list[id].save()
})

//preview
$(".path_design_move_preview_b").on("click", function(e){
	//
	let id = Number($(this).attr("data-id"))
	chain.play(id);
})


$(".path_design_move_save_all_b").on("click", function(e){
	// for all elemnt in list 
	$(".path_design_program_list_b" ).each(function( index){
		let id = $(this).attr("data-id")
		chain.list[id].save()
	})
})

//cancel
$(".path_design_move_cancel_b").on("click", function(e){
	let id = Number($(this).attr("data-id"))
	chain.list[id].cancel()
})

$(".path_design_move_cancel_all_b").on("click", function(e){
	$(".path_design_program_list_b" ).each(function( index){
		let id = $(this).attr("data-id")
		chain.list[id].cancel()
	})
})

// remove
$(".path_design_move_delete_b").on("click", function(e){
	let id = Number($(this).attr("data-id"))
	if (id > 0) {
		chain.list[id].remove()
	}
})

$(".path_design_move_delete_all_b").on("click", function(e){
	chain.delete_all_by_id()
})


// visible
$('.path_design_visible_c').on( 'click', function( e ) {
	let visible = $(this).prop("checked")? true:false
	chain.set_visible(visible)
})
// preview
$('.path_design_preview_b').on( 'click', function( e ) {
	chain.play(0)

})


// change prm
$( ".path_design_prm_v" ).on("change", function(e) {
	let key = $(this).attr("data-key"); 
	let value = Math.abs(Number($(this).prop("value"))) 

	if(! $(`.path_design_prm_c[data-key=${key}]`).prop("checked")){
		$(`.path_design_prm_c[data-key=${key}]`).click();
	}
	//if($(`.path_design_prm_c[data-key=${key}]`).prop("checked")){
	let id = Number($(`.path_design_program_list_b.active`).attr("data-id"))
	if(isNaN(id)){
		return 0
	}
	chain.list[id].prm[key] = value 
	//}
});
//tool head
$( ".ghost_toolhead_v" ).on("change", function(e) {
	chain.tool_head_length_set(Number($(this).prop("value")));
});

// delete prm
$('.path_design_prm_c').on( 'click', function( e ) {
	let id = Number($(`.path_design_program_list_b.active`).attr("data-id"))
	if(isNaN(id)){
		return 0
	}		
	let key = $(this).attr("data-key");
	if(!$(this).prop("checked")){
		delete chain.list[id].prm[key]
	}else{
		let value = Number($(`.path_design_prm_v[data-key=${key}]`).prop("value"))	
		chain.list[id].prm[key] = value
	}

})

// convert to script
$(".path_design_play_b").on("click", function(){
	chain.make_script(true,true) //roundness activated
})


//upload script
$(".make_script_b").on("click", function(){
	chain.make_script(false,true);
})


function change_ghost_value(position){
	$(".path_design_point_prm_v" ).each(function( index){
		$(this).prop("value", position[$(this).attr("data-key")])
	})

	$(".path_design_prm_v" ).each(function( index){
		let key = $(this).attr("data-key")
		if(!(typeof position[$(this).attr("data-key")] === 'undefined')){
			$(`.path_design_prm_c[data-key=${key}]`).prop("checked", true);
			$(this).prop("value", position[key]);
		}
	})

}

function update_path_design_list(id, txt){
	// access the given id
	if(!$(`.path_design_program_list_b[data-id=${id}]`).length){
		$( ".path_design_program_list" ).append( 
			'<button type="button" class="list-group-item list-group-item-action pt-0 pb-0 path_design_program_list_b" data-id="'+id+'">'+txt+'</button>'		
		);
	}
}

function program_list_select(id, point_id, n_points){
	// remove active class
	$(`.path_design_program_list_b`).each(function( index){
		$(this).removeClass( "active" )
	})

	// add active class
	$(`.path_design_program_list_b[data-id=${id}]`).addClass("active")

	// empty select
	$(".path_design_point_s").empty();

	// set path id
	$(".path_design_point_s").attr("data-id", id);

	// active xyz in control type
	$('.path_design_point_ct_r[value=2]').prop('checked', true)

	// ad points to select
	for (let i = 0; i < n_points; i++) {
		$(".path_design_point_s").append(
			"<option value="+i+">point "+i+"</option>"
		)
	}
	/*
	if(n_points == 1){
		$(".custom_control_turn_label").hide()
		$(".custom_control_turn_input").hide()
	}
	else{
		$(".custom_control_turn_label").show()
		$(".custom_control_turn_input").show()
	}
	*/
	//select
	 $('.path_design_point_s').prop( "value", point_id);

	 // ad id to save, cancel, remove
	 $(".path_design_move_save_b").attr("data-id", id)
	 $(".path_design_move_cancel_b").attr("data-id", id)
	 $(".path_design_move_delete_b").attr("data-id", id)
	 $(".path_design_move_preview_b").attr("data-id", id)
	 // add prm
	 let prm = chain.list[id].prm
	 //let cont_prm = chain.list[id].cont_prm
	 // loop over element prm tag and checked and uncheked them
	$(".path_design_prm_c" ).each(function(e){
		let key = $(this).attr("data-key")
		if (key in prm) {
			$(`.path_design_prm_v[data-key=${key}]`).prop("value", prm[key])
			$(this).prop("checked", true)	
		}else{
			$(this).prop("checked", false)			
		}
	})

	if(! ($('.path_design_visible_c').prop("checked"))){
		$('.path_design_visible_c').trigger("click");
	}

	
}


function program_list_remove(id){
	// remove button from list
	$(`.path_design_program_list_b[data-id=${id}]`).remove()

	// select another one
}

var path_upload_promise;

$('.path_upload_b').click(function(e){
	let editor_data = editor.getValue();
	$('.script_upload_b').trigger("click")
	path_upload_promise = new Promise((resolve,reject)=>{
		resolve(editor_data)
	});

})	

$('.path_download_b').click(function(e){

	let editor_data = editor.getValue();
	chain.make_script(false,false);
	$('.script_download_b').trigger("click");
	editor.setValue(editor_data);

})

$('.sync_pos_b').on("click", function(e){
	set_cmd_to_robot()
});

