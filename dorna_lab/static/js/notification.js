//$('.toast_info').toast('show')

function add_note(type) {
	let note = ''
	if (type == "info"){
		let note = '<div class="toast toast_info" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"> <div class="toast-header"> <strong class="mr-auto">Info</strong> <button type="button" class="ml-2 mb-1 close close_toast" data-dismiss="toast" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> <div class="toast-body"> Before start working with the robot make sure that: <ul> <li>All the joints are set to their true values.</li> <li>Motors are enabled.</li> </ul> </div> </div>' 		
		$(".note").append(note)
		$('.toast_info').toast('show')
	}
	else if( type == "joystick_connected"){
		let note = '<div class="toast toast_joystick" role="alert" aria-live="assertive" aria-atomic="true"  data-autohide="false"> <div class="toast-header"> <strong class="mr-auto">Joystick</strong> <button type="button" class="ml-2 mb-1 close close_toast" data-dismiss="toast" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> <div class="toast-body"> Joystick is connected! </div> </div>'
		$(".note").append(note)
		$('.toast_joystick').toast('show')
	}
	else if( type == "joystick_disconnected"){
		let note = '<div class="toast toast_diconnectjoystick" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"> <div class="toast-header"> <strong class="mr-auto">Joystick</strong> <button type="button" class="ml-2 mb-1 close close_toast" data-dismiss="toast" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> <div class="toast-body"> Joystick is disconnected! </div> </div>'
		$(".note").append(note)
		$('.toast_diconnectjoystick').toast('show')
	}
	else if( type == "alarm"){
		let note = '<div class="toast toast_alarm" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"> <div class="toast-header"> <strong class="mr-auto">Alarm</strong> <button type="button" class="ml-2 mb-1 close close_toast" data-dismiss="toast" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> <div class="toast-body"> Disable the alarm first. </div> </div>'
		$(".note").append(note)
		$('.toast_alarm').toast('show')
	}		

}

$(document).on('click', '.close_toast', function(e){ 
	e.preventDefault()
	$(this).closest(".toast").remove()
});

add_note("info")