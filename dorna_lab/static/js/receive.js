//in - out - pwm - adc - path design

// receive message functions
function on_message(event){
  
  try {
    let msg = JSON.parse(event.data);
    for (let key in msg) {
      if(key in function_map) {
        let cmd_key = function_map[key];
        window[cmd_key](key, msg);
      }  
    }
    // print log
    print_log(msg)
  }
  catch(err) {
    console.log("error: ", err, event.data)
  }

}

/* function map methods */

function out_r(key, msg) {
  $(`.out_c[data-key=${key}]`).prop("checked", msg[key]? true:false);
}

function in_r(key, msg) {
  $(`.in_c[data-key=${key}]`).prop("checked", msg[key]? true:false);
}

function pwm_r(key, msg) {
  //key : pwmi
  let index = key.substring(3)
  $(`.dc_v[data-key=${key}]`).prop("value", msg["duty".concat(index)]);
  $(`.freq_v[data-key=${key}]`).prop("value", msg["freq".concat(index)]);
  $(`.pwm_v[data-key=${key}]`).prop("value", msg["pwm".concat(index)]);
}

function adc_r(key, msg) {
  $(`.adc_v[data-key=${key}]`).prop("value", msg[key]);
}

function pid_r(key, msg) {
  $(".pid_v").prop("value", msg[key])
}

function alarm_r(key, msg) {
  let val = msg[key]
  if(val===1){
    $(".alarm_li").show()
    add_note("alarm")  
  }
  if(val===0){
    $(".alarm_li").hide()
    init_ws(args = ["motor", "toollength", "input", "output", "pwm", "adc", "version", "uid"])  
  }  
}

/*
function joint_r(key, msg) {
  $(`.joint_v[data-key=${key}]`).text(Number.parseFloat(msg[key]).toFixed(3));
}
*/

function version_r(key, msg) {
  $(`.version_v[data-key=${key}]`).text(msg[key]);
}

function uid_r(key, msg) {
  $(`.uid_v[data-key=${key}]`).text(msg[key]);
}

function toollength_r(key, msg) {
  $(`.toollength_v[data-key=${key}]`).val(Number.parseFloat(msg[key]).toFixed(2));
  original_robot.tool_head_length_set(Number.parseFloat(msg[key]))
}

function motion_r(key, msg) {
  document.getElementById("motion_r_id_"+key).innerHTML = Number.parseFloat(msg[key]).toFixed(3);
}
/*
function xyz_r(key, msg) {
  $(`.xyz_v[data-key=${key}]`).text(Number.parseFloat(msg[key]).toFixed(3));
}
*/
function encoder_r(key, msg) {
  $(`.encoder_v[data-key=j${key.substring(7)}]`).text(Number.parseFloat(msg[key]).toFixed(2));
}

// motor
function motor_r(key, msg) {
  // replay
  $(".motor_c").prop("checked", msg[key]);
} 

// id
function id_r(key, msg) {
  // replay
  if(msg[key] === 1 && msg["stat"] === 2 && $(".script_replay_c").prop("checked")){
    $(".script_play_b").click()
  } 

  // track 
  if($(".script_track_c").prop("checked") && msg["stat"] === 1 && script_index[0].includes(msg[key] ) ) {
    let id = msg[key]
    let index_id = script_index[0].indexOf(id)

    //remove last highlight
    try {
      script_index[2].clear()
    }
    catch(err) {
    }
    //add new highlight
    script_index[2] = highlight_editor(script_index[1][index_id][0], script_index[1][index_id][1], msg["stat"])

    
    // update script index
    script_index[0].splice(index_id, 1)
    script_index[1].splice(index_id, 1)
  } 
}

function print_log(msg){

  if("cmd" in msg && msg["cmd"] == "motion"){
    return 0
  }
  log_print(JSON.stringify(msg), "🟢")  

}