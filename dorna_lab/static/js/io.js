$('.out_c').on( 'click', function( e ) {
	e.preventDefault()
	let msg = {
		"cmd":  $(this).attr("data-cmd"),
	}
	msg[$(this).attr("data-key")] = $(this).prop("checked")? 1:0
	send_message(msg)
})

$('.in_c').on( 'click', function( e ) {
	e.preventDefault()
})

$(".pwm_b").on("click", function(e) {
	e.preventDefault()

	let msg = {"cmd":  $(this).attr("data-cmd")}
	let key = $(this).attr("data-key");
	msg[key] = parseInt($(this).prop("value"))

	msg[$(`.dc_n[data-key=${key}]`).attr("data-item")] = Number($(`.dc_n[data-key=${key}]`).prop("value"))
	
	msg[$(`.freq_n[data-key=${key}]`).attr("data-item")] = Number($(`.freq_n[data-key=${key}]`).prop("value"))
	send_message(msg)
});
